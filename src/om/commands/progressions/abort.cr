require "../../commands"
require "../../common"
require "../../data_types"
require "optarg"
require "time"

include OM::Common

class AbortModel < Optarg::Model
  bool %w(-h --help), default: false
end

class OM::Commands::Progression::Abort < OM::Command
  @name = "abort"
  @alias = %w(cancel a c)
  @use = "Abort a progression"

  def initialize : Nil
  end

  def invoke(argv : Array(String), command = self) : Nil
    result = StartModel.parse argv
    if result.h? || result.help?
      help
      exit 0
    end

    # Change HEAD
    head = get_head
    head.step = nil
    write_head head
    puts "Progression Aborted"
  end

  def help : Nil
    puts @use
  end
end
