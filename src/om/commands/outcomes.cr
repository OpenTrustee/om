require "../parent_command"
require "./outcomes/*"

class OM::Commands::Outcomes < OM::ParentCommand
  @name = "outcome"
  @alias = %w(outcomes o)
  @use = "Create, edit, list and more for outcomes"
  @default_command = "list"
end
