require "exception"
require "optarg"
require "levenshtein"
require "./common"
require "./command"

class SubCommandParser < Optarg::Model
  arg "sub_command", default: "", stop: true
end

class OM::ParentCommand < OM::Command
  @not_found_message = "Command not found."
  @default_command = "help"
  @commands = [] of OM::Command

  def initialize(@commands : Array(OM::Command))
  end

  def find_command(name : String) : OM::Command | Nil
    @commands.each { |cmd| return cmd if cmd.name == name || cmd.alias.includes? name }
    nil
  end

  def get_parsed_argv(argv : Array(String))
    result = SubCommandParser.parse argv
    result
  end

  def invoke(argv : Array(String))
    result = get_parsed_argv argv
    sub_command = result.sub_command

    # Initialise the help command
    help_command = OM::Commands::Help.new @commands

    # Attempt to find a matching sub-command
    command = find_command(sub_command)

    if command.nil? && sub_command.size == 0
      if @default_command != "help"
        command = find_command(@default_command)
      end
    end

    if command
      if !OM::Common.project_initialised && command.needs_init == true
        STDERR.puts "Project has not yet been initialised"
        exit 1
      end

      command.invoke result.unparsed_args
    else
      # Compare commands to try to determine which the user meant
      possible_commands = [] of String
      @commands.each do |command|
        distance = Levenshtein.distance(sub_command, command.name)
        if 0 < distance < 3
          possible_commands << command.name
        end
      end

      puts ""
      puts "OM".colorize.mode(:bold).to_s + " v#{OM::Common.get_version}"
      if @name.size > 0
        puts "#{@name.colorize.mode(:underline).to_s} — #{@use}"
      end
      help_command.invoke argv
      puts "\n"

      puts "#{@not_found_message}".colorize(:red).to_s
      if possible_commands.size != 0
        puts "Were you looking for '#{possible_commands.join("' or '")}'?".colorize(:cyan).to_s
      end
      exit 1
    end
  end
end
