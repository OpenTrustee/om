module OM::Templates
  extend self

  def outcome_msg : String
    <<-eof
# Title Goes Below Here

# Descripton Goes Below Here


# Lines starting with "#" are ignored, just like this one.
#
# OM is designed to help you think more positively and have you working
# towards your goals faster. You can think about outcomes as what it looks like
# when you've finished a task. For example "Dishwasher Running" is an outcome
# of you having rinsed your dishes, stacked your dishwasher, loaded the
# detergent and pressed the start button.
#
# In the above scenario you might have an tree of outcomes like this:
# Kitchen Clean
#   |
#   +- Benches Clean
#   +- Dishwasher Running
#        |
#        +- Dishwasher Stacked
#        +- Detergent Present
#   +- Dishes Rinsed
#
# You can see how exciting it is to think about the end result of all your hard
# work than it might be to think about what you've got left to do.
#
# In the above example each item in the tree has a title you can see, but also
# a description which is not shown. An example description for the "Detergent
# Present" outcome might be:
#
# A single Finish dishwashing table is located in the closed detergent
# receptacle.
#
# Again see how the description describes the outcome as if it has already
# happened? This is the key to thinking properly in OM.
#
# Once you've added an outcome you can start progressing towards achieving
# your outcomes. Hooray!
eof
  end
end
